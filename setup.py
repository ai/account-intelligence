#!/usr/bin/python

from setuptools import setup, find_packages

setup(
    name="account_intelligence",
    version="0.1",
    description="A/I Account Sensors",
    author="Autistici/Inventati",
    author_email="info@autistici.org",
    url="https://git.autistici.org/ai/account-intelligence",
    install_requires=['tensorflow'],
    setup_requires=[],
    zip_safe=True,
    packages=find_packages(),
    package_data={},
    entry_points={
        "console_scripts": [
            "account-intelligence-nn = account_intelligence.net:main",
            ],
        },
    )

