#!/usr/bin/python

import math
import json
import optparse
import os
import sys
import tensorflow as tf
import numpy as np


def read_lines(fd):
    # Skip empty lines and comments.
    for line in fd:
        if not line or line.startswith('#'):
            continue
        yield line


def read_training_set(filename):
    # Training file format:
    # <user> <is_spammer:{0,1}>
    user_spamminess = {}
    with open(filename) as fd:
        for line in read_lines(fd):
            username, is_spammer = line.split()
            user_spamminess[username] = int(is_spammer)
    return user_spamminess


def build_training_set(usermap, spammers):
    """Build the training labels array.

    Args:
      usermap: a {index: user} dict (or list)
      spammers: a {user: is_spammer} dict

    """
    result = []
    for user in usermap:
        is_spammer = spammers.get(user, False)
        if is_spammer:
            result.append([0, 1])
        else:
            result.append([1, 0])
    return np.array(result)


def read_input(input_fd):
    for line in read_lines(input_fd):
        yield json.loads(line)


def make_domain_map(rows):
    # Create a domain -> index map.
    all_domains = set()
    for row in rows:
        all_domains.update(row.get('top10_domains', {}).iterkeys())
    return dict((d, i) for i, d in enumerate(all_domains))


def domains_to_vector(d, domain_map, n_msgs=1):
    """Build a vector from a domain dict.

    Args:
      d: a {domain: count} dict with hit counts
      domain_map: a {domain: column_index} map
      n_msgs: if not 1, it is expected to be the total number of messages,
        so that the resulting array is normalized

    """
    out = [float(0)] * len(domain_map)
    for domain, count in d.iteritems():
        if domain not in domain_map:
            continue
        out[domain_map[domain]] = float(count) / n_msgs
    return out


def row_to_feature(row, domain_map):
    """Build a feature row from an input row."""
    n_msgs = row.get('messages_count', 0)
    n_bounces = row.get('bounces_count', 0)
    feature_row = [
        math.log10(1 + n_msgs),
        math.log10(1 + row.get('freemail_sum', 0)),
        math.log10(1 + row.get('ratelimit_rate_count', 0)),
        math.log10(1 + row.get('ratelimit_rcpt_count', 0)),
    ]
    feature_row.extend(
        domains_to_vector(row.get('top_domains', {}), domain_map, n_msgs))
    feature_row.extend(
        domains_to_vector(row.get('top_bounces', {}), domain_map, n_bounces))
    return feature_row


def load_input_data(input_fd, domain_map=None):
    """Load the input data.

    If domain_map is None, also build the domain map - this
    unfortunately requires two scans on the input, so we're going to
    have to load it all in memory in that case.

    Returns:
      A (user_map, domain_map, features) tuple, where:
      - user_map maps row indexes to users
      - domain_map maps domains to column indexes
      - features is the input feature matrix

    """
    users_map = []
    features = []
    rows = read_input(input_fd)
    if domain_map is None:
        rows = list(rows)
        domain_map = make_domain_map(rows)
    for row in rows:
        users_map.append(row['user'])
        features.append(row_to_feature(row, domain_map))
    return users_map, domain_map, np.array(features)


def multilayer_perceptron(x, weights, biases):
    """Build a two-layer fully-connected mesh of nodes."""
    activation = tf.nn.sigmoid
    #activation = tf.nn.relu
    layer_1 = tf.add(tf.matmul(x, weights['w1']), biases['b1'])
    layer_1 = activation(layer_1)
    layer_2 = tf.add(tf.matmul(layer_1, weights['w2']), biases['b2'])
    layer_2 = activation(layer_2)
    layer_3 = tf.add(tf.matmul(layer_2, weights['w3']), biases['b3'])
    layer_3 = activation(layer_3)
    out_layer = tf.matmul(layer_3, weights['out']) + biases['out']
    return out_layer


def build_network(n_features, n_classes):
    n_hidden_1 = 20
    n_hidden_2 = 10
    n_hidden_3 = 4

    weights = {
        'w1': tf.Variable(tf.random_normal([n_features, n_hidden_1]), name='w1'),
        'w2': tf.Variable(tf.random_normal([n_hidden_1, n_hidden_2]), name='w2'),
        'w3': tf.Variable(tf.random_normal([n_hidden_2, n_hidden_3]), name='w3'),
        'out': tf.Variable(tf.random_normal([n_hidden_3, n_classes]), name='wout'),
    }
    biases = {
        'b1': tf.Variable(tf.random_normal([n_hidden_1]), name='b1'),
        'b2': tf.Variable(tf.random_normal([n_hidden_2]), name='b2'),
        'b3': tf.Variable(tf.random_normal([n_hidden_3]), name='b3'),
        'out': tf.Variable(tf.random_normal([n_classes]), name='bout'),
    }

    inputs = tf.placeholder(tf.float32, [None, n_features])
    training = tf.placeholder(tf.uint8, [None, n_classes])
    pred = multilayer_perceptron(inputs, weights, biases)
    
    return inputs, training, pred


def train(input_fd, spammers, state_dir):
    learning_rate = 0.0002
    training_epochs = 20000
    display_step = training_epochs / 10

    users, domain_map, features = load_input_data(input_fd)
    labels = build_training_set(users, spammers)

    n_classes = len(labels[0])
    n_features = len(features[0])
    n_rows = len(features)
    print 'training on %d samples of %d features' % (n_rows, n_features)

    inputs, training, pred = build_network(n_features, n_classes)
    cost = tf.reduce_mean(
        tf.nn.softmax_cross_entropy_with_logits(logits=pred, labels=training))
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

    init = tf.global_variables_initializer()
    saver = tf.train.Saver()
    with tf.Session() as sess:
        sess.run(init)

        for i in xrange(training_epochs):
            sess.run(optimizer, feed_dict={inputs: features, training: labels})
            if (i) % display_step == 0:
                cc = sess.run(cost, feed_dict={inputs: features, training: labels})
                print "Training step:", '%04d' % (i), "cost=", cc

        correct_prediction = tf.equal(tf.argmax(pred, 1), tf.argmax(training, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, 'float'))
        print 'Accuracy:', accuracy.eval({inputs: features, training: labels})

        output_path = saver.save(sess, os.path.join(state_dir, 'tf.ckpt'))

    return domain_map


def classify(input_fd, domain_map, spammers, state_dir):
    users, _, features = load_input_data(input_fd, domain_map)
    if spammers:
        labels = build_training_set(users, spammers)
    n_features = len(features[0])
    n_classes = 2

    inputs, training, pred = build_network(n_features, n_classes)
    
    saver = tf.train.Saver()
    init = tf.global_variables_initializer()
    with tf.Session() as sess:
        sess.run(init)
        saver.restore(sess, os.path.join(state_dir, 'tf.ckpt'))

        results = sess.run(pred, feed_dict={inputs: features})

        for idx, row in enumerate(results):
            print users[idx], row, sess.run(tf.argmax(row, 0))

        if spammers:
            correct_prediction = tf.equal(tf.argmax(pred, 1), tf.argmax(training, 1))
            accuracy = tf.reduce_mean(tf.cast(correct_prediction, 'float'))
            print 'Accuracy:', accuracy.eval({inputs: features, training: labels})


def main():
    parser = optparse.OptionParser()
    parser.add_option('--state-dir', dest='state_dir',
                      default='.')
    parser.add_option('--train', action='store_true')
    parser.add_option('--labels', dest='labels_file',
                      default='train.dat')
    opts, args = parser.parse_args()

    if len(args) == 0:
        input_fd = sys.stdin
    elif len(args) == 1:
        input_fd = open(args[0])
    else:
        parser.error('Too many arguments')

    domain_map_file = os.path.join(opts.state_dir, 'domain_map.json')

    if opts.train:
        spammers = read_training_set(opts.labels_file)
        domain_map = train(input_fd, spammers, opts.state_dir)
        with open(domain_map_file, 'w') as fd:
            json.dump(domain_map, fd)
    else:
        try:
            spammers = read_training_set(opts.labels_file)
        except:
            spammers = None
        with open(domain_map_file) as fd:
            domain_map = json.load(fd)
        classify(input_fd, domain_map, spammers, opts.state_dir)


if __name__ == '__main__':
    main()

