// loganalyzer
//
// Generate high-level events from mail.log files.
//
// Includes a number of processing entities such as Analyzers, that
// can detect full SASL-authenticated email transactions to identify
// message flows, and Aggregators that can compute aggregate per-user
// statistics.
//
package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"flag"
	"io"
	"log"
	"os"
	"regexp"
	"runtime"
	"sort"
	"strings"
	"sync"
	"time"
)

var (
	minMessageCount       = flag.Int("min-messages", 3, "minimum number of messages that a user must have sent in order to be included in the output")
	topDestinationDomains = flag.Int("top-domains", 10, "number of top destination domains to retain per user")
	freemailDomainsFile   = flag.String("freemail-domains", "", "file containing a list of freemail domains, one per line")
)

// Process events in batches when possible, to minimize context switching.
const chanSize = 100

// Static list of freemail domains. Note that the string can match
// anywhere in the domain (makes it easier to handle localized domains
// like hotmail and yahoo).
var freemailDomains = []string{
	"hotmail.",
	"yahoo.",
	"gmail.com",
	"ymail.com",
	"aol.com",
	"aim.com",
	"msn.com",
	"att.net",
	"verizon.net",
	"comcast.net",
	"mail.ru",
	"orange.fr",
	"sky.com",
	"free.fr",
	"libero.it",
	"tiscali.it",
}

func isFreemail(domain string) bool {
	for _, needle := range freemailDomains {
		if strings.Contains(domain, needle) {
			return true
		}
	}
	return false
}

func loadFreemailDomains(path string) error {
	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	var tmp []string
	for scanner.Scan() {
		tmp = append(tmp, strings.TrimSpace(scanner.Text()))
	}
	freemailDomains = tmp
	return scanner.Err()
}

// Events are just generic JSON-serializable maps.
type event map[string]interface{}

func getStringField(e event, field string) (string, bool) {
	value, ok := e[field].(string)
	return value, ok
}

func getIntField(e event, field string) (int, bool) {
	value, ok := e[field].(int)
	return value, ok
}

type actionFunc func([][]byte) event

// An action is a regexp with an associated function. If the regexp matches a
// log line, the function will be called with the matched subexpressions as
// arguments. Regexps are matched against the syslog message contents (no
// timestamp, no host).
type action struct {
	rx *regexp.Regexp
	f  actionFunc
}

// A module provides a number of regexp-based actions, possibly sharing the same
// (hidden) state.
type module interface {
	actions() []action
}

// State for matching traffic flows.
type flowEntry struct {
	user   string
	from   string
	to     string
	bounce bool
}

func (e flowEntry) done() bool {
	return e.user != "" && e.from != "" && e.to != ""
}

// The trafficFlowModule detects SASL-authenticated message flows and emits
// 'flow' events accordingly.
type trafficFlowModule struct {
	mx    sync.Mutex
	state map[string]*flowEntry
}

func newTrafficFlowModule() *trafficFlowModule {
	return &trafficFlowModule{
		state: make(map[string]*flowEntry),
	}
}

func (m *trafficFlowModule) actions() []action {
	// Follow the message flow through the various Postfix
	// daemons. Note that the input might not be temporally
	// sorted, so the handlers are going to emit events only when
	// we have the full information about a message.
	return []action{
		// SASL-authenticated SMTP connection.
		{regexp.MustCompile(`^postfix/smtpd\[\d+\]: ([A-F0-9]{11}): client=.*, sasl_username=(.*)$`), m.handleUserLogin},
		// Postfix queue manager receives the message and tells us
		// the sender.
		{regexp.MustCompile(`^postfix/qmgr\[\d+\]: ([A-F0-9]{11}): from=<([^>]+)>`), m.handleFrom},
		// Outbound SMTP client has successfully sent a message.
		{regexp.MustCompile(`^postfix/smtp\[\d+\]: ([A-F0-9]{11}): to=<([^>]+)>,.*status=sent`), m.handleTo},
		// A remote MX told us that the message is spam or that the
		// destination does not exist.
		{regexp.MustCompile(`^postfix/smtp\[\d+\]: ([A-F0-9]{11}): to=<([^>]+)>,.*status=bounced.* (550|553|554) `), m.handleBounce},
		// Rejected emails are also to be considered in stats.
		{regexp.MustCompile(`^postfix/smtpd\[\d+\]: ([A-F0-9]{11}): reject: .* to=<([^>]+)>`), m.handleTo},
	}
}

func (m *trafficFlowModule) getEntry(id string) *flowEntry {
	e, ok := m.state[id]
	if !ok {
		e = &flowEntry{}
		m.state[id] = e
	}
	return e
}

func (m *trafficFlowModule) sendIfDone(id string, e *flowEntry) event {
	if !e.done() {
		return nil
	}

	delete(m.state, id)
	eventType := "flow"
	if e.bounce {
		eventType = "bounce"
	}
	return event{
		"type": eventType,
		"user": e.user,
		"from": e.from,
		"to":   e.to,
	}
}

func (m *trafficFlowModule) handleUserLogin(matches [][]byte) event {
	id := string(matches[1])
	if id == "" {
		return nil
	}

	m.mx.Lock()
	defer m.mx.Unlock()

	e := m.getEntry(id)
	e.user = string(matches[2])
	return m.sendIfDone(id, e)
}

func (m *trafficFlowModule) handleFrom(matches [][]byte) event {
	id := string(matches[1])
	if id == "" {
		return nil
	}

	m.mx.Lock()
	defer m.mx.Unlock()

	e := m.getEntry(id)
	e.from = string(matches[2])
	return m.sendIfDone(id, e)
}

func (m *trafficFlowModule) handleBounce(matches [][]byte) event {
	id := string(matches[1])
	if id == "" {
		return nil
	}

	m.mx.Lock()
	defer m.mx.Unlock()

	e := m.getEntry(id)
	e.to = string(matches[2])
	e.bounce = true
	return m.sendIfDone(id, e)
}

func (m *trafficFlowModule) handleTo(matches [][]byte) event {
	id := string(matches[1])
	if id == "" {
		return nil
	}

	m.mx.Lock()
	defer m.mx.Unlock()

	e := m.getEntry(id)
	e.to = string(matches[2])
	return m.sendIfDone(id, e)
}

// The postfwdRatelimitModule tracks users that hit the Postfwd rate
// limits when attempting to send messages.
type postfwdRatelimitModule struct{}

func newPostfwdRatelimitModule() *postfwdRatelimitModule {
	return &postfwdRatelimitModule{}
}

func (m *postfwdRatelimitModule) actions() []action {
	return []action{
		// Matches A/I postfwd rules (RCPT_SASL or RATE_SASL),
		// which identify two separate rate limit classes.
		{regexp.MustCompile(`^postfwd2/policy\[\d+\]: \[RULES\] .*id=([A-Z]+)_SASL.*user=([^,]+),.*action=4\d\d`), m.handlePostfwd},
	}
}

func (m *postfwdRatelimitModule) handlePostfwd(matches [][]byte) event {
	return event{
		"type": "ratelimit_" + strings.ToLower(string(matches[1])),
		"user": string(matches[2]),
	}
}

// Load JSON-encoded events from an input stream.
func loadEvents(r io.Reader) <-chan event {
	c := make(chan event, chanSize)
	go func() {
		defer close(c)

		scanner := bufio.NewScanner(r)
		for scanner.Scan() {
			var obj map[string]interface{}
			if err := json.Unmarshal(scanner.Bytes(), obj); err == nil {
				c <- obj
			}
		}
	}()
	return c
}

// Dump JSON-encoded events to an output stream.
func dumpEvents(c <-chan event) {
	enc := json.NewEncoder(os.Stdout)
	for e := range c {
		enc.Encode(e)
	}
}

// A transformer can modify events during the analysis phase.
type transformer interface {
	transform(event) event
}

// An analyzer accumulates metrics and outputs them at the end as a
// set of different aggregated events.
type analyzer interface {
	analyze(event)
	done(chan event)
}

// A typeFilter wraps an analyzer by only selecting events of a
// specific type.
type typeFilter struct {
	t    string
	wrap analyzer
}

func newTypeFilter(t string, wrap analyzer) *typeFilter {
	return &typeFilter{t: t, wrap: wrap}
}

func (a *typeFilter) analyze(e event) {
	t, ok := getStringField(e, "type")
	if !ok || t != a.t {
		return
	}
	a.wrap.analyze(e)
}

func (a *typeFilter) done(outC chan event) {
	a.wrap.done(outC)
}

// The rateAnalyzer computes event rates by accumulating samples in
// 'period'-sized buckets.
type rateAnalyzer struct {
	period  int64 // seconds
	buckets map[int64]int64
}

func (a *rateAnalyzer) analyze(e event) {
	b := e["stamp"].(time.Time).Unix() / a.period
	a.buckets[b]++
}

// Analyze distribution of values for a map field with string keys and
// int values.
type fieldDistributionAnalyzer struct {
	field        string
	valuesByUser map[string]map[string]int
}

func newFieldDistributionAnalyzer(field string) *fieldDistributionAnalyzer {
	return &fieldDistributionAnalyzer{
		field:        field,
		valuesByUser: make(map[string]map[string]int),
	}
}

func (a *fieldDistributionAnalyzer) analyze(e event) {
	value, ok := getStringField(e, a.field)
	if !ok {
		return
	}
	user, ok := getStringField(e, "user")
	if !ok {
		return
	}

	values, ok := a.valuesByUser[user]
	if !ok {
		values = make(map[string]int)
		a.valuesByUser[user] = values
	}
	values[value]++
}

func (a *fieldDistributionAnalyzer) done(outC chan event) {
}

// The domainDistributionAnalyzer computes the list of top-10
// destination domains for each user, and counts how many messages
// were delivered to freemail domains.
type domainDistributionAnalyzer struct {
	*fieldDistributionAnalyzer
	topN  int
	field string
}

func newDomainDistributionAnalyzer(field string, topN int) *domainDistributionAnalyzer {
	return &domainDistributionAnalyzer{
		fieldDistributionAnalyzer: newFieldDistributionAnalyzer("to_domain"),
		topN:  topN,
		field: field,
	}
}

func (a *domainDistributionAnalyzer) done(outC chan event) {
	for user, values := range a.valuesByUser {
		// Just keep the top 10 destination domains.
		top := topN(values, a.topN)

		outC <- event{
			"user":  user,
			a.field: top,
		}
	}
}

// freemailChecker processes flow events, setting the 'freemail' field
// to 1 if the destination is a freemail domain.
type freemailChecker struct{}

func newFreemailChecker() *freemailChecker {
	return &freemailChecker{}
}

func (t *freemailChecker) transform(e event) event {
	if domain, ok := getStringField(e, "to_domain"); ok && isFreemail(domain) {
		e["freemail"] = 1
	}
	return e
}

// An intSummerAnalyzer simply sums up (per-user) integer values from
// a single event field.
type intSummerAnalyzer struct {
	field     string
	sumByUser map[string]int
}

func newIntSummerAnalyzer(field string) *intSummerAnalyzer {
	return &intSummerAnalyzer{
		field:     field,
		sumByUser: make(map[string]int),
	}
}

func (a *intSummerAnalyzer) analyze(e event) {
	value, ok := getIntField(e, a.field)
	if !ok {
		return
	}
	user, ok := getStringField(e, "user")
	if !ok {
		return
	}

	a.sumByUser[user] += value
}

func (a *intSummerAnalyzer) done(outC chan event) {
	outField := a.field + "_sum"
	for user, sum := range a.sumByUser {
		outC <- event{
			"user":   user,
			outField: sum,
		}
	}
}

// A counterAnalyzer accumulates per-user event counters.
type counterAnalyzer struct {
	tag         string
	countByUser map[string]int
}

func newCounterAnalyzer(tag string) *counterAnalyzer {
	return &counterAnalyzer{
		tag:         tag,
		countByUser: make(map[string]int),
	}
}

func (a *counterAnalyzer) analyze(e event) {
	user, ok := getStringField(e, "user")
	if !ok {
		return
	}
	a.countByUser[user]++
}

func (a *counterAnalyzer) done(outC chan event) {
	outField := a.tag + "_count"
	for user, count := range a.countByUser {
		outC <- event{
			"user":   user,
			outField: count,
		}
	}
}

// extractToDomain is a transformer that extracts the destination
// domain from message flow events.
type extractToDomain struct{}

func newDestinationDomainExtractor() *extractToDomain {
	return &extractToDomain{}
}

func (t *extractToDomain) transform(e event) event {
	if to, ok := getStringField(e, "to"); ok {
		e["to_domain"] = strings.ToLower(strings.Split(to, "@")[1])
	}
	return e
}

// Read and parse mail.log input. Modules will generate events on the
// returned channel, which will be closed once all input has been
// processed.
func readLog(r io.Reader, modules []module) <-chan event {
	var actions []action
	for _, mod := range modules {
		modActions := mod.actions()
		actions = append(actions, modActions...)
	}

	// Read input lines.
	inpC := make(chan []byte, chanSize)
	go func() {
		defer close(inpC)
		scanner := bufio.NewScanner(r)
		for scanner.Scan() {
			// Bytes() returns a shared buffer, so we need to
			// make a copy of it.
			b := scanner.Bytes()
			line := make([]byte, len(b))
			copy(line, b)
			inpC <- line
		}
		if err := scanner.Err(); err != nil {
			log.Printf("input error: %v", err)
		}
	}()

	var wg sync.WaitGroup
	c := make(chan event, chanSize)
	now := time.Now()

	// Spawn a number of regexp workers to make use of all available cores.
	for i := 0; i < runtime.NumCPU(); i++ {
		wg.Add(1)
		go func() {
			for line := range inpC {
				stamp, err := time.Parse(time.Stamp, string(line[:15]))
				if err != nil {
					continue
				}
				// Get rid of the host field. Saves a massive
				// amount of CPU time by allowing simpler
				// regular expressions.
				nextSpc := 17 + bytes.IndexByte(line[16:], ' ')
				msg := line[nextSpc:]
				stamp = stamp.AddDate(now.Year(), 0, 0)
				for _, action := range actions {
					m := action.rx.FindSubmatch(msg)
					if len(m) > 0 {
						if e := action.f(m); e != nil {
							// Add a timestamp to all events.
							e["stamp"] = stamp.UTC()
							c <- e
						}
						break
					}
				}
			}
			wg.Done()
		}()
	}

	go func() {
		wg.Wait()
		close(c)
	}()

	return c
}

// Apply transformers and analyzers to a stream of events. The output
// is a stream of aggregated events.
func analyzeEvents(c <-chan event, transforms []transformer, analyzers []analyzer) <-chan event {
	outC := make(chan event, chanSize)
	go func() {
		for e := range c {
			for _, t := range transforms {
				e = t.transform(e)
			}
			for _, a := range analyzers {
				a.analyze(e)
			}
		}
		for _, a := range analyzers {
			a.done(outC)
		}
		close(outC)
	}()
	return outC
}

func mergeEvents(a, b event) event {
	if a == nil {
		return b
	}
	for k, v := range b {
		a[k] = v
	}
	return a
}

// groupBy groups events by the specified key and merges them. The
// expectations is that events with the same key will have different
// attributes (for instance because they come from separate analysis
// pipelines).
func groupBy(key string, c <-chan event) <-chan event {
	outC := make(chan event, chanSize)
	go func() {
		g := make(map[string]event)
		for e := range c {
			value, ok := getStringField(e, key)
			if !ok {
				log.Printf("ignored event with no grouping key")
				continue
			}
			g[value] = mergeEvents(g[value], e)
		}
		for _, e := range g {
			outC <- e
		}
		close(outC)
	}()
	return outC
}

// Only output events when the specified field is above a certain
// threshold.
func thresholdFilter(field string, threshold int, c <-chan event) <-chan event {
	outC := make(chan event, chanSize)
	go func() {
		for e := range c {
			value, ok := getIntField(e, field)
			if !ok || value < threshold {
				continue
			}
			outC <- e
		}
		close(outC)
	}()
	return outC
}

type valuePair struct {
	k string
	v int
}

type valuePairList []valuePair

func (l valuePairList) Len() int           { return len(l) }
func (l valuePairList) Swap(i, j int)      { l[i], l[j] = l[j], l[i] }
func (l valuePairList) Less(i, j int) bool { return l[i].v > l[j].v }

func topN(values map[string]int, n int) map[string]int {
	var tmp []valuePair
	for k, v := range values {
		tmp = append(tmp, valuePair{k, v})
	}
	sort.Sort(valuePairList(tmp))
	if len(tmp) > n {
		tmp = tmp[:n]
	}

	topN := make(map[string]int)
	for _, p := range tmp {
		topN[p.k] = p.v
	}
	return topN
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	// Allow users to provide their own freemail domain list.
	if *freemailDomainsFile != "" {
		if err := loadFreemailDomains(*freemailDomainsFile); err != nil {
			log.Fatal(err)
		}
	}

	// Read and parse logs from standard input.
	inp := readLog(os.Stdin, []module{
		newTrafficFlowModule(),
		newPostfwdRatelimitModule(),
	})

	// Build the analysis pipeline:
	//
	// First, we augment mail flow events with information on the
	// destination domain, using event transformers.
	//
	// Then we aggregate the following information by user over
	// the whole input dataset:
	//
	// - total number of messages sent
	// - message counts for the top 10 destination domains
	// - message counts for the top 10 failed destination domains
	// - number of messages sent to freemail domains
	// - number of times the user has been ratelimited by postfwd
	//
	// Finally, we only output events for users that have sent at
	// least 3 messages.
	//
	// Internally to the pipeline, all events must have a 'user' key
	// (that is what aggregating analyzers expect).
	//
	dumpEvents(thresholdFilter("messages_count", *minMessageCount, groupBy("user", analyzeEvents(
		inp,
		[]transformer{
			newDestinationDomainExtractor(),
			newFreemailChecker(),
		},
		[]analyzer{
			newIntSummerAnalyzer("freemail"),
			newTypeFilter("flow", newCounterAnalyzer("messages")),
			newTypeFilter("bounce", newCounterAnalyzer("bounces")),
			newTypeFilter("flow", newDomainDistributionAnalyzer("top_domains", *topDestinationDomains)),
			newTypeFilter("bounce", newDomainDistributionAnalyzer("top_bounces", *topDestinationDomains)),
			newTypeFilter("ratelimit_rate", newCounterAnalyzer("ratelimit_rate")),
			newTypeFilter("ratelimit_rcpt", newCounterAnalyzer("ratelimit_rcpt")),
		},
	))))
}
